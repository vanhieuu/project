import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";
import { ColorSchemeName } from "react-native";
import GestureFlipView from "react-native-gesture-flip-card";
import InputScreen from "../screens/InputScreen";
import NotFoundScreen from "../screens/NotFoundScreen";
import OutPutScreen from "../screens/OutPutScreen";
import { RootStackParamList } from "../types";
import BottomTabNavigator from "./BottomTabNavigator";
import LinkingConfiguration from "./LinkingConfiguration";
const Stack = createStackNavigator<RootStackParamList>();
// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
export default function Navigation({
  colorScheme,
}: {
  colorScheme: ColorSchemeName;
}) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
    >
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        {/* <Stack.Screen name="Root" component={BottomTabNavigator} /> */}
        <Stack.Screen name="InputScreen" component={InputScreen} />
        <Stack.Screen name="OutPutScreen" component={OutPutScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
