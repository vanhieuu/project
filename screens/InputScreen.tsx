import React, { useState, useCallback } from "react";
import { StyleSheet, Text, View, TextInput, Button } from "react-native";
import { IUser, RootStackParamList } from "../types";
import { StackNavigationProp } from "@react-navigation/stack";
import { TouchableOpacity } from "react-native-gesture-handler";

type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  "InputScreen"
>;

const dataOption = [
  {
    id: 0,
    title: "Option 1",
    value: 0,
  },
  {
    id: 1,
    title: "Option 2",
    value: 1,
  },
  {
    id: 2,
    title: "Option 3",
    value: 2,
  },
  {
    id: 3,
    title: "Option 4",
    value: 3,
  },
];
const convertStringDate = (timeSstamp:number):string =>{
    return;
}
type Props = {
  navigation: ProfileScreenNavigationProp;
};
const InputScreen = ({ navigation }: Props) => {
  const [user, setUser] = useState<IUser>({
    name: "",
    age: "",
  });

  const setAge = useCallback((age: string) => {
    setUser(
      (prev: IUser): IUser => {
        return {
          ...prev,
          age,
        };
      }
    );
  }, []);
  const setUserInfo = useCallback((value: string, type: string) => {
    setUser(
      (prev: IUser): IUser => {
        return {
          ...prev,
          [type]: value,
        };
      }
    );
  }, []);
  const [currentSelectIndex,setCurrentSelectIndex] = useState(-1);
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <View style={{ marginBottom: 16 }}>
        <Text style={{ fontSize: 16, marginBottom: 12 }}>Name</Text>
        <TextInput
          placeholder="enter Name"
          value={user.name}
          onChangeText={(value: string) => {
            setUserInfo(value, "name");
          }}
        />
      </View>
      <View style={{ marginBottom: 16 }}>
        <Text style={{ marginBottom: 16, fontSize: 16 }}>Age</Text>
        <TextInput
          placeholder="enter age"
          value={user.age}
          onChangeText={(value: string) => {
            setUserInfo(value, "age");
          }}
          // onChangeText={setAge}
        />
      </View>
      {dataOption.map((item,index) =>{
          let isSelect:boolean = index === currentSelectIndex
          return(
        <TouchableOpacity key={index} onPress={() =>{
            setCurrentSelectIndex(index)
        }} style={{
            backgroundColor: isSelect ? 'red' :'#fff',
            flexDirection:'row'

        }}>
            <Text>
                {item.title}
            </Text>
        </TouchableOpacity>
      )
      })}
        
      <Button
        title={`enter ${currentSelectIndex}`}
        onPress={() => {
          console.log("user", user);
          navigation.navigate("OutPutScreen", { user });
        }}
      />
    </View>
  );
};

export default InputScreen;
const timeDifferent = (current:number, previous:number):string =>{
    var msPerMin = 60*1000;
    var msPerHour = msPerMin*60;
    var msPerDay = msPerHour*24;
    var msPerMon = msPerDay*30;
    var msPerYear = msPerMon*12
    var elapsed = current - previous;
            if(elapsed < msPerMin){
                return Math.round(elapsed/1000)+ "second ago"
            } else if (elapsed < msPerHour){
                return  Math.round(elapsed/msPerMin) + "min ago"
            }else if (elapsed <msPerDay){
                return Math.round(elapsed/msPerHour) + "hour ago"
            }else if(elapsed<msPerMon){
                return "approximately" + Math.round(elapsed/msPerDay) + "days ago"
            }

} 

const styles = StyleSheet.create({});
