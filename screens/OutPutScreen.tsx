import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { RouteProp } from '@react-navigation/native';
import { RootStackParamList } from '../types';

type ProfileScreenRouteProp = RouteProp<RootStackParamList, 'OutPutScreen'>;

type Props = {
  route: ProfileScreenRouteProp;
  backgroundColor:string;
};
const OutPutScreen = ({route, backgroundColor='pink'} :Props) => {
    const user = route.params.user
    return (
        <View style={{
            flex:1,
            justifyContent:'center',
            alignItems:'center',
            paddingLeft:12,
            backgroundColor
        }}>
            <Text>Name:{user.name}</Text>
            <Text>Age:{user.age}</Text>
        </View>
    )
}

export default OutPutScreen

const styles = StyleSheet.create({})
