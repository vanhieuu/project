export interface IUser {
  name: string;
  age: string;
}
export type RootStackParamList = {
  InputScreen: undefined;
  OutPutScreen: {user:IUser};
  NotFound: undefined
};

export type BottomTabParamList = {
  TabOne: undefined;
  TabTwo: undefined;
};

export type TabOneParamList = {
  TabOneScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};
